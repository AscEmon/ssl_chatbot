import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ssl_chatbot/ssl_chatbot.dart';

void main() {
  const MethodChannel channel = MethodChannel('ssl_chatbot');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });
}
