#import "SslChatbotPlugin.h"
#if __has_include(<ssl_chatbot/ssl_chatbot-Swift.h>)
#import <ssl_chatbot/ssl_chatbot-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "ssl_chatbot-Swift.h"
#endif

@implementation SslChatbotPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftSslChatbotPlugin registerWithRegistrar:registrar];
}
@end
